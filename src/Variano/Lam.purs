module Variano.Lam where

import Data.Function (($))
import Data.Functor (class Functor)
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, SProxy(SProxy), inj)
import Type.Row (type (+))
import Variano (MuV)

newtype LAM a
  = LAM
    { body :: a
    , variable :: String
    }

derive instance functorLAM :: Functor LAM

type Lam r
  = (lam :: FProxy LAM | r)

lam :: forall r. String -> MuV (Lam + r) -> MuV (Lam + r)
lam variable body = In (inj (SProxy :: SProxy "lam") $ LAM {body, variable})
