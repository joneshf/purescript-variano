module Variano.Abs where

import Data.Function (($))
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, SProxy(SProxy), inj)
import Data.Identity (Identity(..))
import Type.Row (type (+))
import Variano (MuV)

type Abs r
  = (abs :: FProxy Identity | r)

abs :: forall r. MuV (Abs + r) -> MuV (Abs + r)
abs x = In (inj (SProxy :: SProxy "abs") $ Identity x)
