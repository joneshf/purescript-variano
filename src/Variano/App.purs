module Variano.App where

import Data.Function (($))
import Data.Functor (class Functor)
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, SProxy(SProxy), inj)
import Type.Row (type (+))
import Variano (MuV)

newtype APP a
  = APP
    { argument :: a
    , function :: a
    }

derive instance functorAPP :: Functor APP

type App r
  = (app :: FProxy APP | r)

app :: forall r. MuV (App + r) -> MuV (App + r) -> MuV (App + r)
app function argument =
  In (inj (SProxy :: SProxy "app") $ APP {argument, function})
