module Variano.DeBruijn where

import Data.Const (Const(..))
import Data.Function (($))
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, SProxy(SProxy), inj)
import Type.Row (type (+))
import Variano (MuV)

type DEBRUIJN
  = Const Int

type DeBruijn r
  = (deBruijn :: FProxy DEBRUIJN | r)

deBruijn :: forall r. Int -> MuV (DeBruijn + r)
deBruijn x = In (inj (SProxy :: SProxy "deBruijn") $ Const x)
