module Variano.Desugar where

import Prelude

import Data.Const (Const(..))
import Data.Foldable (foldr)
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (onMatch)
import Data.List (elemIndex)
import Data.List.Types (List(..), (:))
import Data.Maybe (maybe)
import Type.Row (type (+))
import Variano (MuV)
import Variano.Abs (Abs)
import Variano.Abs as Variano.Abs
import Variano.App (APP(..), App)
import Variano.App as Variano.App
import Variano.DeBruijn (DeBruijn)
import Variano.DeBruijn as Variano.DeBruijn
import Variano.Lam (LAM(..), Lam)
import Variano.Let (Let)
import Variano.Let as Variano.Let
import Variano.Lets (LETS(..), Lets)
import Variano.Var (Var, VAR)
import Variano.Var as Variano.Var

-- | Since we can't fully know that there are no free variables,
-- | we have to account for them and have `Var` along with `DeBruijn`.
type AlmostNameless r
  = (Abs + App + DeBruijn + Var + r)

-- | Desugars lambdas with named variables to abstractions and deBruijn indices.
-- |
-- | E.g. If we had:
-- |
-- | ```PureScript
-- | lam "x" (lam "y" (var "x"))
-- | ```
-- |
-- | it would desugar to:
-- |
-- | ```PureScript
-- | abs (abs (deBruijn 1))
-- | ```
namedLambdas ::
  forall r.
  MuV (AlmostNameless + App + Lam + Var + r) ->
  MuV (AlmostNameless + r)
namedLambdas = go Nil
  where
  -- | Given a naming context, replace (almost) all of the variables.
  -- | This is actually pretty elegant, but can't remember where I first saw it.
  -- | I take no credit for this algorithm.
  -- |
  -- | The index in the list works for shadowing and different levels.
  -- | E.g. If we had:
  -- |
  -- | ```PureScript
  -- | lam "x" (lam "x" (var "x"))
  -- | ```
  -- |
  -- | it would (correctly) desugar to:
  -- |
  -- | ```PureScript
  -- | abs (abs (deBruijn 0))
  -- | ```
  go ::
    List (VAR (MuV (AlmostNameless + App + Lam + Var + r))) ->
    MuV (AlmostNameless + App + Lam + Var + r) ->
    MuV (AlmostNameless + r)
  go context (In x) = onMatch {app, lam, var} (In <<< map (go context)) x
    where
    app ::
      APP (MuV (AlmostNameless + App + Lam + Var + r)) ->
      MuV (AlmostNameless + r)
    app (APP {argument, function}) =
      Variano.App.app (go context function) (go context argument)
    lam ::
      LAM (MuV (AlmostNameless + App + Lam + Var + r)) ->
      MuV (AlmostNameless + r)
    lam (LAM {body, variable}) =
      Variano.Abs.abs (go (Const variable : context) body)
    var ::
      VAR (MuV (AlmostNameless + App + Lam + Var + r)) ->
      MuV (AlmostNameless + r)
    var v@(Const str) =
      maybe (Variano.Var.var str) Variano.DeBruijn.deBruijn (elemIndex v context)

-- | Desugars one let with multiple bindings
-- | to multiple lets each with one binding.
-- |
-- | E.g. if we had:
-- |
-- | ```PureScript
-- | lets
-- |   ( {binding: "x", expression: int 1}
-- |   :| Cons {binding: "y", expression: int 2} Nil
-- |   )
-- |   (var "x")
-- | ```
-- |
-- | it would desugar to:
-- |
-- | ```PureScript
-- | let "x" (int 1) (let "y" (int 2) (var "x"))
-- | ```
letWithMultipleBindings :: forall r. MuV (Let + Lets + r) -> MuV (Let + r)
letWithMultipleBindings (In x) =
  onMatch {lets} (In <<< map letWithMultipleBindings) x
    where
    lets :: LETS (MuV (Let + Lets + r)) -> MuV (Let + r)
    lets (LETS {bindings, body}) =
      foldr go (letWithMultipleBindings body) bindings
    go ::
      {binding :: String, expression :: MuV (Let + Lets + r)} ->
      MuV (Let + r) ->
      MuV (Let + r)
    go {binding, expression} =
      Variano.Let.let' binding (letWithMultipleBindings expression)
