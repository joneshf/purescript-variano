module Variano.Let where

import Data.Function (($))
import Data.Functor (class Functor)
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, inj)
import Data.Symbol (SProxy(..))
import Type.Row (type (+))
import Variano (MuV)

newtype LET a
  = LET
    { binding :: String
    , body :: a
    , expression :: a
    }

derive instance functorLET :: Functor LET

type Let r
  = (let :: FProxy LET | r)

let' :: forall r. String -> MuV (Let + r) -> MuV (Let + r) -> MuV (Let + r)
let' binding expression body =
  In (inj (SProxy :: SProxy "let") $ LET {binding, body, expression})
