module Variano.Lets where

import Data.Function (($))
import Data.Functor (class Functor)
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, inj)
import Data.List.Types (List)
import Data.NonEmpty (NonEmpty)
import Data.Symbol (SProxy(..))
import Type.Row (type (+))
import Variano (MuV)

newtype LETS a
  = LETS
    { bindings :: NonEmpty List {binding :: String, expression :: a}
    , body :: a
    }

derive instance functorLETS :: Functor LETS

type Lets r
  = (lets :: FProxy LETS | r)

lets ::
  forall r.
  NonEmpty List {binding :: String, expression :: MuV (Lets + r)} ->
  MuV (Lets + r) ->
  MuV (Lets + r)
lets bindings body =
  In (inj (SProxy :: SProxy "lets") $ LETS {bindings, body})
