module Variano.Var where

import Data.Const (Const(..))
import Data.Function (($))
import Data.Functor.Mu (Mu(..))
import Data.Functor.Variant (FProxy, SProxy(SProxy), inj)
import Type.Row (type (+))
import Variano (MuV)

type VAR
  = Const String

type Var r
  = (var :: FProxy VAR | r)

var :: forall r. String -> MuV (Var + r)
var str = In (inj (SProxy :: SProxy "var") $ Const str)
