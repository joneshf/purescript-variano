module Variano where

import Data.Functor.Mu (Mu)
import Data.Functor.Variant (VariantF)

type MuV r
  = Mu (VariantF r)
